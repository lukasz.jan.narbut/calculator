package pl.narbut.calculator.app;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pl.narbut.calculator.app.dto.CalculatorRequest;
import pl.narbut.calculator.app.dto.CalculatorResponse;

@RestController
@RequestMapping("/calculator")
@AllArgsConstructor
public class CalculatorController {

    private final CalculatorService service;

    @GetMapping("/div")
    public CalculatorResponse div(@RequestBody final CalculatorRequest request) {
        double result = service.div(request.getVal1(), request.getVal2());
        return CalculatorResponse.builder()
                .value(result)
                .build();
    }

    @PostMapping("/add")
    public CalculatorResponse add(@RequestBody final CalculatorRequest request) {
        double result = service.add(request.getVal1(), request.getVal2());
        return CalculatorResponse.builder()
                .value(result)
                .build();
    }

}
