package pl.narbut.calculator.app;

import org.springframework.stereotype.Service;

@Service
public class CalculatorService {

    static final String CAN_NOT_DIVIDE_BY_ZERO_EXCEPTION = "Can not divide by zero";

    public double add(final double firstNumber, final double secondNumber) {
        return firstNumber + secondNumber;
    }

    public double div(final double firstNumber, final double secondNumber) {
        if (secondNumber == 0) {
            throw new BusinessException(CAN_NOT_DIVIDE_BY_ZERO_EXCEPTION);
        }
        return firstNumber / secondNumber;
    }


}
