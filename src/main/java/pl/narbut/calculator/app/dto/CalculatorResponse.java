package pl.narbut.calculator.app.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CalculatorResponse {

    private double value;

}
