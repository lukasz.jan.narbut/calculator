package pl.narbut.calculator.app;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.function.Predicate;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;

class CalculatorServiceTest {

    CalculatorService sut = new CalculatorService();

    @Test
    void should_successfully_perform_addition_operation() {
        double firstNumber = 1.0;
        double secondNumber = 0.2;
        double addResult = sut.add(firstNumber, secondNumber);
        double expected = 1.2;
        assertThat(addResult).isEqualTo(expected);
    }

    @Test
    void should_successfully_perform_divide_operation() {
        double firstNumber = 3.0;
        double secondNumber = 2.0;
        double divResult = sut.div(firstNumber, secondNumber);
        double expected = 1.5;
        assertThat(divResult).isEqualTo(expected);
    }

    @Test
    void should_throw_exception_for_divide_when_second_number_is_zero() {
        double firstNumber = 3.0;
        double secondNumber = 0.0;
        assertThatThrownBy(() -> sut.div(firstNumber, secondNumber))
                .isInstanceOf(BusinessException.class)
                .hasMessage(CalculatorService.CAN_NOT_DIVIDE_BY_ZERO_EXCEPTION);
    }
}