package pl.narbut.calculator.app;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import pl.narbut.calculator.app.dto.CalculatorRequest;
import pl.narbut.calculator.app.dto.CalculatorResponse;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(CalculatorController.class)
class CalculatorControllerTest {

    @MockBean
    private CalculatorService service;
    @SpyBean
    private ObjectMapper objectMapper;
    @SpyBean
    private BusinessExceptionAdvice businessExceptionAdvice;
    @Autowired
    private MockMvc sut;

    @Test
    void should_successfully_additional_values() throws Exception {
        final double firstNumber = 1.0;
        final double secondNumber = 2.0;
        final double result = 3.0;

        when(service.add(firstNumber, secondNumber)).thenReturn(result);

        sut.perform(post("/calculator/add")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(createRequestInJson(firstNumber, secondNumber))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().string(createResultInJson(result)));

    }

    @Test
    void should_successfully_divide_values() throws Exception {
        final double firstNumber = 3.0;
        final double secondNumber = 1.5;
        final double result = 2.0;

        when(service.div(firstNumber, secondNumber)).thenReturn(result);

        sut.perform(get("/calculator/div")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(createRequestInJson(firstNumber, secondNumber))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().string(createResultInJson(result)));

    }

    @Test
    void should_throw_exception_for_divide_when_second_number_is_zero() throws Exception {
        final double firstNumber = 3.0;
        final double secondNumber = 0.0;
        final String exceptionMessage = CalculatorService.CAN_NOT_DIVIDE_BY_ZERO_EXCEPTION;

        when(service.div(firstNumber, secondNumber)).thenThrow(new BusinessException(exceptionMessage));

        sut.perform(get("/calculator/div")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(createRequestInJson(firstNumber, secondNumber))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(exceptionMessage));
    }

    @Test
    void should_assign_zero_when_value_is_null() throws Exception {
        final double firstNumber = 1.0;
        final double result = 1.0;
        final double defaultValueForNullValue = 0.0;

        when(service.add(firstNumber, defaultValueForNullValue)).thenReturn(result);

        CalculatorRequest request = CalculatorRequest.builder()
                .val1(firstNumber)
                .build();

        sut.perform(post("/calculator/add")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().string(createResultInJson(result)));
    }

    private String createResultInJson(final double result) throws JsonProcessingException {
        CalculatorResponse response = CalculatorResponse.builder()
                .value(result)
                .build();
        return objectMapper.writeValueAsString(response);
    }

    private String createRequestInJson(final double firstNumber, final double secondNumber) throws JsonProcessingException {
        CalculatorRequest request = CalculatorRequest.builder()
                .val1(firstNumber)
                .val2(secondNumber)
                .build();
        return objectMapper.writeValueAsString(request);
    }

}